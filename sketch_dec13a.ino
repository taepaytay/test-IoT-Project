#include <Firebase.h>
#include <FirebaseArduino.h>
#include <FirebaseCloudMessaging.h>
#include <FirebaseError.h>
#include <FirebaseHttpClient.h>
#include <FirebaseObject.h>
#include <ESP8266WiFi.h>
#include <time.h>
//#include "DHT.h"

#define timezone 7
#define RLED D1
#define GLED D2

#define DBUTT D4
#define L1BUTT D8
#define L2BUTT D3

#define RELAYDoor D5
#define RELAYL1 D6
#define RELAYL2 D7
//#define DHTPIN    D8
//#define DHTTYPE   DHT11

//DHT dht(DHTPIN, DHTTYPE);

const char* ssid     = ""; //Wifi ssid
const char* password = ""; //Wifi Password

#define FIREBASE_HOST "" //Firebase Data-base URL
#define FIREBASE_KEY "" //Firebase Key

long check = 0, dlay = 0, DHTReadDlay = 0;
int Dstatus, DstatusTemo, L1status, L2status, DbuttState, L1buttState, L2buttState;
float humid, temp;

void setup()
{
  pinMode(RLED,OUTPUT);
  pinMode(GLED,OUTPUT);
  pinMode(DBUTT, INPUT);
  pinMode(L1BUTT, INPUT);
  pinMode(L2BUTT, INPUT); 
  pinMode(RELAYDoor, OUTPUT);
  pinMode(RELAYL1, OUTPUT);
  pinMode(RELAYL2, OUTPUT);
//  dht.begin();
  
  Serial.begin(115200);
  Serial.println("Starting...");
  
  if (WiFi.begin(ssid, password)) {
    Serial.print("Connecting WiFi ");
    while (WiFi.status() != WL_CONNECTED) {   
        delay(1000);
        Serial.print(".");
    }
  }
  Serial.print("DONE\n");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  configTime(timezone * 3600, 0, "pool.ntp.org", "time.nist.gov");
  Serial.print("Get time ");
  while (!time(nullptr)) {
    Serial.print(".");
    delay(1000);
  }
  Serial.print(" DONE\n");

  Serial.print("Connect firebase..");
  Firebase.begin(FIREBASE_HOST, FIREBASE_KEY);
  Serial.print(" DONE\n");
  Serial.println("\n**************Connect done!!**************\n");

  digitalWrite(RLED, LOW);
  digitalWrite(GLED, HIGH);
  digitalWrite(RELAYDoor, HIGH);
  digitalWrite(RELAYL1, HIGH);
  digitalWrite(RELAYL2, HIGH);
  
  time_t now = time(nullptr);
  Firebase.set("myLab/last update", ctime(&now));
  Dstatus = Firebase.getBool("myLab/DoorStatus");
  DstatusTemo = Dstatus;
  
//  Serial.print(Dstatus);
//  Serial.println(DstatusTemo);
}

void openDoor(){
  dlay = millis();
  Serial.print("Open\n");
//  Serial.print(Dstatus);
//  Serial.println(DstatusTemo);
  digitalWrite(GLED, HIGH);
  digitalWrite(RLED, LOW);
  digitalWrite(RELAYDoor, LOW);
}

void closeDoor(){
  digitalWrite(GLED, LOW);
  digitalWrite(RLED, HIGH);
  digitalWrite(RELAYDoor, HIGH);
}

void loop()
{
  if(millis() - check > 100){
    check = millis();
    
    DbuttState = digitalRead(DBUTT);
    L1buttState = digitalRead(L1BUTT);
    L2buttState = digitalRead(L2BUTT);
    
    Dstatus = Firebase.getBool("myLab/DoorStatus");
    L1status = Firebase.getBool("myLab/Light1Stauts");
    L2status = Firebase.getBool("myLab/Light2Stauts");
    time_t now = time(nullptr);
    
    if(millis() - DHTReadDlay > 2000){
        Firebase.set("myLab/last update", ctime(&now));
        Serial.println(ctime(&now));
        DHTReadDlay = millis();
//      humid = dht.readHumidity();
//      temp  = dht.readTemperature();
    }
    
    Serial.print("Door ");
    Serial.println(DbuttState);
    Serial.print("Light1 ");
    Serial.println(L1status);
    Serial.print("Light2 ");
    Serial.println(L2status);

//    if (isnan(humid) || isnan(temp)) {
//      Serial.println("Failed to read from DHT sensor!\n");
//    }else{
//      Firebase.setInt("myLab/temp", temp);
//      Firebase.setInt("myLab/humid", humid);
//      Serial.print("Temp: ");  
//      Serial.print(temp); 
//      Serial.print(" C, ");
//      Serial.print("Humid: "); 
//      Serial.print(humid); 
//      Serial.println(" %");
//    }

    if(L1status){
      digitalWrite(RELAYL1, LOW);
    }else{
      digitalWrite(RELAYL1, HIGH);
    }

    if(L1buttState){
      Firebase.setBool("myLab/Light1Stauts", true);
    }else{
      Firebase.setBool("myLab/Light1Stauts", false);
    }

    if(L2status){
      digitalWrite(RELAYL2, LOW);
    }else{
      digitalWrite(RELAYL2, HIGH);
    }

    if(L2buttState){
      Firebase.setBool("myLab/Light2Stauts", true);
    }else{
      Firebase.setBool("myLab/Light2Stauts", false);
    }
    
    if(Dstatus != DstatusTemo){
      openDoor();
      DstatusTemo = Dstatus;
    }
    
    else if(DbuttState){
      openDoor();
      
    }else{
      if(millis() - dlay > 3000){
        closeDoor();
      }
    }
  }
}
